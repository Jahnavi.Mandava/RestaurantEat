package com.example.sharp.restauranteat

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    var btnSignIn: Button?=null
    var btnSign:Button?=null
    var textDesc: TextView?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnSignIn = findViewById<Button>(R.id.btnSignIn) as Button
        btnSign = findViewById<Button>(R.id.btnSign) as Button
        textDesc = findViewById<TextView>(R.id.textDesc) as TextView
        btnSignIn?.setOnClickListener(object: View.OnClickListener {
            override  fun onClick(view:View) {
            }
        })
        btnSign?.setOnClickListener(object: View.OnClickListener {
            override  fun onClick(view:View) {
            }
        })
    }
}
